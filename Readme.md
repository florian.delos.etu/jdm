# Journée des Masters

## Présentation

Le but de la journée des Masters est de présenter les différents Masters proposés par l'Université de Lille à ses étudiants au sein de la Faculté des Sciences et Technologies au département Informatique. Dans le cadre de cette journée les Étudiants de Master 2 vous présenterons leurs Projets de Fin d'Études.

Cette journée sera l'occasion de répondre aux questions que vous vous posez telles que :

-   Quels métiers possibles ?
-   Quelles sont les compétences acquises ?
-   Quelles sont les spécialités proposées ?
-   Peut-on travailler pendant les études ?
-   À quoi ressemble le marché du travail dans ce domaine ?


## Planning

Le *jeudi 13 février* sur le Campus Cité Scientifique de Villeneuve d'Ascq.

Heure|Évènement|Lieu
:---:|:---:|:---:
10h20|Présentation des Masters|C1 Amphi Kuhlman (pour les L3, M1) & M1 Amphi Châtelet (pour les L2)
11h15|Démonstrations des projets|M5 Salles de spécialités
13h30|Présentation de l'alternance|M1 Amphi Painlevé
15h30|![Forum de l'alternance](https://sciences-technologies.univ-lille.fr/index.php?id=1996)|

## Plan d'accès

![Plan d'accès](Images/Plan.jpg)
